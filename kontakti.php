<?php
	require_once 'core/db.php';

    $title = "Контакты | SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве";
    $desc = "SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве";
    $keys = "#скидки, #акции, #распродажи, #купоны, #промокоды, #скидкимосква, #скидкикраснодар, #акциикраснодар, #акциимосква, #распродажикраснодар, #распродажимосква";


	require_once 'tpl/header.tpl';
?>



<main class="site-main  main-container no-sidebar">
        <div class="container">
			<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"><h2 class="trail-browse">Путь:</h2><ul class="trail-items breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="trail-item trail-begin"><a href="/" rel="home"><span itemprop="name">Главная</span></a><meta itemprop="position" content="1" /></li><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="trail-item trail-end active"><span itemprop="name">Контакты</span><meta itemprop="position" content="2" /></li></ul></nav>            <div class="row">
                <div class="main-content col-sm-12">
                    <h2 class="page-title">
						Наши контакты                    </h2>
					                            <div class="page-main-content">
								<div class="vc_row wpb_row vc_row-fluid ovic_custom_5b7badbd70f6e"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-xs ovic_custom_5b7badbd70f96 vc_custom_1518079313101"><div class="vc_column-inner "><div class="wpb_wrapper"><h3 style="font-size: 18px" class="vc_custom_heading ovic_custom_5b7badbd70fdb vc_custom_1521691334338" >Местоположение</h3></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding ovic_custom_5b7badbd71020"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">            <div class="ovic-google-maps  ovic_custom_5b7badbd7106f vc_custom_1515133981243 "
                 id="az-google-maps-5b964d90a79a5"
                 style="min-height: 500px">
            </div>
            <script type="text/javascript">
                window.addEventListener('load',
                    function (ev) {
                        var $hue             = '',
                            $saturation      = '',
                            $modify_coloring = false,
                            $ovic_map        = {
                                lat: 45.072035,
                                lng: 38.9879407                            };
                        if ( $modify_coloring === true ) {
                            var $styles = [
                                {
                                    stylers: [
                                        {hue: $hue},
                                        {invert_lightness: false},
                                        {saturation: $saturation},
                                        {lightness: 1},
                                        {
                                            featureType: "landscape.man_made",
                                            stylers: [ {
                                                visibility: "on"
                                            } ]
                                        }
                                    ]
                                }, {
                                    featureType: 'water',
                                    elementType: 'geometry',
                                    stylers: [
                                        {color: '#46bcec'}
                                    ]
                                }
                            ];
                        }
                        var map = new google.maps.Map(document.getElementById("az-google-maps-5b964d90a79a5"), {
                            zoom: 17,
                            center: $ovic_map,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            styles: $styles
                        });

                        var contentString = '<div style="background-color:#fff; padding: 30px 30px 10px 25px; width:290px;line-height: 22px" class="ovic-map-info">' +
                            '<h4 class="map-title">sale.guide</h4>' +
                            '<div class="map-field"><i class="fa fa-map-marker"></i><span>Краснодар. Ростовское шоссе 14Б</span></div>' +
                            '<div class="map-field"><i class="fa fa-phone"></i><span><a href="tel:8-8005501451">8-800-550-14-51</a></span></div>' +
                            '<div class="map-field"><i class="fa fa-envelope"></i><span><a href="mailto:saleguide.ru@gmail.com">saleguide.ru@gmail.com</a></span></div> ' +
                            '</div>';

                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });

                        var marker = new google.maps.Marker({
                            position: $ovic_map,
                            map: map
                        });
                        marker.addListener('click', function () {
                            infowindow.open(map, marker);
                        });
                    }, false);
            </script>
			</div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>

			<br><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6 ovic_custom_5b7badbd710b3 vc_custom_1515118652828"><div class="vc_column-inner "><div class="wpb_wrapper"><h3 style="font-size: 18px" class="vc_custom_heading ovic_custom_5b7badbd710f3 vc_custom_1521691348084" >Отправьте нам сообщение</h3><p  class="vc_custom_heading ovic_custom_5b7badbd71133 vc_custom_1521691355839" >Указывайте правильный Email, так как на него придет ответ</p><div role="form" class="wpcf7" id="wpcf7-f3146-p2588-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="javascript:;" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="3146" />
<input type="hidden" name="_wpcf7_version" value="5.0.4" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3146-p2588-o1" />
<input type="hidden" name="_wpcf7_container_post" value="2588" />
</div>
<p class="text-short left">
    <label><br />
        <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Имя*" /></span><br />
    </label>
</p>
<p class="text-short right">
    <label><br />
        <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email*" /></span><br />
    </label>
</p>
<p>
    <label><br />
        <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Тема*" /></span><br />
    </label>
</p>
<p>
    <label><br />
        <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Сообщение*"></textarea></span><br />
    </label>
</p>
<p>
    <input type="submit" value="Отправить" class="wpcf7-form-control wpcf7-submit" /></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-1 vc_col-md-1 vc_hidden-md vc_hidden-sm vc_hidden-xs ovic_custom_5b7badbd711b8"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-6 ovic_custom_5b7badbd711fa vc_custom_1515125917556"><div class="vc_column-inner "><div class="wpb_wrapper"><h3 style="font-size: 18px" class="vc_custom_heading ovic_custom_5b7badbd7123a vc_custom_1521691362230" >Контактная информация</h3><p  class="vc_custom_heading ovic_custom_5b7badbd7128b vc_custom_1521691370153" >Добро пожаловать на наш сайт. Мы рады, что вы с нами</p><div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text ovic_custom_5b7badbd712cc ovic_custom_5b7badbd712cc ovic_custom_5b7badbd712cc" ><span class="vc_sep_holder vc_sep_holder_l"><span  style="border-color:#e6e6e6;" class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  style="border-color:#e6e6e6;" class="vc_sep_line"></span></span>
</div><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6 ovic_custom_5b7badbd7130c"><div class="vc_column-inner "><div class="wpb_wrapper">            <div class="ovic-iconbox style1  ovic_custom_5b7badbd7134c ">
                <div class="iconbox-inner">
											                            <div class="icon">
                                <span class="fa fa-phone"></span>
                            </div>
											                    <div class="content">
						                                <h4 class="title">Телефон</h4>
							                            <p class="text">8-800-550-14-51</p>
						                    </div>
                </div>
            </div>
			</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">            <div class="ovic-iconbox style1  ovic_custom_5b7badbd7137e ">
                <div class="iconbox-inner">
											                            <div class="icon">
                                <span class="fa fa-envelope-o"></span>
                            </div>
											                    <div class="content">
						                                <h4 class="title">Email</h4>
							                            <p class="text">saleguide.ru@gmail.com</p>
						                    </div>
                </div>
            </div>
			</div></div></div></div><div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text ovic_custom_5b7badbd713c0 ovic_custom_5b7badbd713c0 ovic_custom_5b7badbd713c0" ><span class="vc_sep_holder vc_sep_holder_l"><span  style="border-color:#e6e6e6;" class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  style="border-color:#e6e6e6;" class="vc_sep_line"></span></span>
</div>            <div class="ovic-iconbox style1  ovic_custom_5b7badbd71406 ">
                <div class="iconbox-inner">
											                            <div class="icon">
                                <span class="fa fa-map-marker"></span>
                            </div>
											                    <div class="content">
						                            <p class="text">350000. Краснодар. <br>Ростовское шоссе 14Б. пн-пт с 9-00 до 18-00</p>
						                    </div>
                </div>
            </div>
			<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text ovic_custom_5b7badbd71446 ovic_custom_5b7badbd71446 ovic_custom_5b7badbd71446" ><span class="vc_sep_holder vc_sep_holder_l"><span  style="border-color:#e6e6e6;" class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  style="border-color:#e6e6e6;" class="vc_sep_line"></span></span>
</div>            <div class="ovic-socials widget-socials  ovic_custom_5b7badbd71485 vc_custom_1515720089442 ">
                <div class="content-socials"><br>
					                        <ul class="socials-list">

					                        	<li>
                                        <a target="_blank" href="https://vk.com/sale.guide">
                                            <span class="fa fa-vk" style="color: #4a76a8;"></span>
											VK                                        </a>
                                    </li>
															                                    
																							                                    <li>
                                        <a target="_blank" href="https://www.facebook.com/SaleGuide-321722558393355/?modal=admin_todo_tour">
                                            <span class="fa fa-facebook" style="color: #1a0dab;"></span>
											Facebook                                        </a>
                                    </li>
																							                                    <li>
                                        <a target="_blank" href="https://www.instagram.com/sale.guide/">
                                            <span class="fa fa-instagram" style="color: #B3348D;"></span>
											Instagram                                        </a>
                                    </li>
																							                                    
																							                                    <li>
                                        <a target="_blank" href="https://www.youtube.com/channel/UCW5XIK5reQMr4G3DTVsroXQ?view_as=subscriber">
                                            <span class="fa fa-youtube" style="color: #F8251C;"></span>
											YouTube                                        </a>
                                    </li>
																							                                    
															                        </ul>
					                </div>
            </div>
			</div></div></div></div>
                            </div>
														                </div>
				            </div>
        </div>
    </main>




<?
	require_once 'tpl/footer.tpl';
?>