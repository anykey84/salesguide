<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$title_page = "Список розыгрешей";

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li class="active">Розыгрыши</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>

			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                            	<div class="btn-group pull-right m-t-15">
                                    <a href="/control/roz-new.php" class="btn btn-default dropdown-toggle waves-effect waves-light">Добавить</a>
                                </div>

                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="1">ID</th>
                                    <th>Название</th>
                                    <th>Категории</th>
                                    <th width="1" class="text-center">Действия</th>
                                </tr>
                                </thead>


                                <tbody>

                                <? $n=0; $sql_opt = mysql_query("SELECT * FROM `roz_cat` ORDER BY `id` ASC");
                            	while ($sql = mysql_fetch_assoc($sql_opt)) { $n++;
                                    $sub = mysql_result(mysql_query("SELECT COUNT(*) FROM `roz_podcat` WHERE `id_cat` = '$sql[id]'"), 0);
                                ?>
	                                <tr>
                                        <td><?=$sql['id'];?></td>
	                                    <td><?=$sql['name'];?></td>
                                        <?/*<td><a href="/control/listovka-photo.php?id=<?=$sql['id'];?>">Всего: <?=$sub;?></a></td>*/?>
                                        <td><a href="/control/roz-podcat.php?id=<?=$sql['id'];?>">Всего: <?=$sub;?></a></td>
	                                    <td class="text-center">
	                                    	<a href="/control/edit-roz.php?id=<?=$sql['id'];?>" class="btn btn-primary btn-custom btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Редактировать"><i class="md md-border-color"></i></a>&nbsp;&nbsp;&nbsp;
	                                    	<a href="javascript:;" onclick="del('<?=$sql['id'];?>');" class="btn btn-danger btn-custom btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Удалить"><i class="md md-delete"></i></a>
	                                    </td>
	                                </tr>
                            	<? } ?>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>




                    </div> <!-- container -->
                               
                </div> <!-- content -->

   	<script type="text/javascript">
    	$(document).ready(function () {
        	$('#datatable').dataTable();
        });

        function del(id) {
	        if (confirm('Подтверждаете удаление?')) {
	            location.href = '/control/process/del-roz.php?id='+id;
	            return true;
	        } else {
	            return false;
	        }
    	}
    </script>

<?
	require_once H.'assets/tpl/footer.tpl';
?>