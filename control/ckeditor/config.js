CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.filebrowserUploadUrl = 'upload.php';
	config.filebrowserBrowseUrl = '/gallery/ckupload';
};
