<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$get = filter($_GET['id']);
	$get_sql = mysql_fetch_assoc(mysql_query("SELECT * FROM `category` WHERE `id` = '$get' LIMIT 1"));

	$title_page = "Добавление категории";

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li><a href="/control/all-category.php">Категории</a></li>';
	$arr_navig[] = '<li>'.$get_sql['name'].'</li>';
	$arr_navig[] = '<li class="active">Добавление подкатегории</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>

			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        	<div class="col-lg-6">
								<div class="card-box">
									<form class="form-horizontal" role="form" method="post" action="/control/process/new-subcategory.php?id=<?=$get_sql['id'];?>" enctype="multipart/form-data">
										<div class="form-group">
											<label for="I1" class="col-sm-4 control-label">Название</label>
											<div class="col-sm-7">
												<input type="text" id="I1" class="form-control" placeholder="Название" name="name" required autocomplete="off">
											</div>
										</div>
										



										<div class="form-group">
											<div class="col-sm-offset-4 col-sm-8">
												<button type="submit" class="btn btn-primary waves-effect waves-light">
													Добавить
												</button>
												<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
													Отмена
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>



                    </div> <!-- container -->
                               
                </div> <!-- content -->


<?
	require_once H.'assets/tpl/footer.tpl';
?>