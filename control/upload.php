<?php

    function watermark($file, $watermark) {

        if (empty($file) || empty($watermark)) {
            return false;
        }

        $wh = getimagesize($watermark);
        $fh = getimagesize($file);
        $rwatermark = imagecreatefrompng($watermark);
        $rfile = imagecreatefromjpeg($file);

        imagecopy($rfile, $rwatermark, $fh[0] - $wh[0], $fh[1] - $wh[1], 0, 0, $wh[0], $wh[1]);
        imagejpeg($rfile, $file, '80');
        imagedestroy($rwatermark);
        imagedestroy($rfile);

        return true;

    }

function translit($in) {
    $trans1= array("'",'`',',',' ',"?","!",":",";",".","Ё","Ж","Ч","Ш","Щ","Э","Ю","Я","ё","ж","ч","ш","щ","э","ю","я","А","Б","В","Г","Д","Е","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ь","Ы","а","б","в","г","д","е","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ь","ы");
    $trans2= array('-','-','-','-',"","","","",".","JO","ZH","CH","SH","SCH","Je","Jy","Ja","jo","zh","ch","sh","sch","je","jy","ja","A","B","V","G","D","E","Z","I","J","K","L","M","N","O","P","R","S","T","U","F","H","C","","Y","a","b","v","g","d","e","z","i","j","k","l","m","n","o","p","r","s","t","u","f","h","c","","y");
    return str_replace($trans1,$trans2,$in);
}



    $callback = $_GET['CKEditorFuncNum'];
    $file_name1 = rand(9999, 99999999)."_".translit($_FILES['upload']['name']);
    $file_name = mb_strtolower($file_name1, 'UTF-8');
    $file_name_tmp = $_FILES['upload']['tmp_name'];
    $file_new_name = '../gallery/upload-ck/';
    $full_path = $file_new_name.$file_name;
    $http_path = 'http://'.$_SERVER['HTTP_HOST'].'/gallery/upload-ck/'.$file_name;
    $error = '';

    if(move_uploaded_file($file_name_tmp, $full_path) ) {
    } else {
     $error = 'Some error occured please try again later';
     $http_path = '';
    }

   // watermark($file_name_tmp, "../../../themes/img/wm.png");

    echo "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );</script>";
?>