<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$title_page = "Добавление фотографий";

	$get = filter($_GET['id']);
	$get_sql = mysql_fetch_assoc(mysql_query("SELECT * FROM `listovki_podcat` WHERE `id` = '$get' LIMIT 1"));
	$mag = mysql_fetch_assoc(mysql_query("SELECT * FROM `listovki_cat` WHERE `id` = '$get_sql[id_cat]' LIMIT 1"));

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li><a href="/control/listovki-all.php">Листовки</a></li>';
	$arr_navig[] = '<li><a href="/control/listovka-podcat.php?id='.$mag['id'].'">'.$mag['name'].'</a></li>';
	$arr_navig[] = '<li><a href="/control/listovka-photo.php?id='.$get.'">'.$get_sql['name'].'</a></li>';
	$arr_navig[] = '<li class="active">Добавление фото</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>

			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        	<div class="col-lg-12">
								<div class="card-box">
									<form class="form-horizontal" role="form" method="post" action="/control/process/new-phlist.php?id=<?=$get;?>" enctype="multipart/form-data">
										

										<div class="form-group">
	                                        <label class="col-md-2 control-label">Выберите фото</label>
	                                        <div class="col-md-6">
	                                        	<input type="file" class="filestyle" data-input="false" name="file[]" multiple="true" required>
	                                        </div>
	                                    </div>





										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" class="btn btn-primary waves-effect waves-light">
													Загрузить фото
												</button>
												<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
													Отмена
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>



                    </div> <!-- container -->
                               
                </div> <!-- content -->


<script type="text/javascript">
	$("#cat").change(function() {
		//var fn = [$("#cat option:selected").val()];
		var fn = $('#cat').val();
		console.log(fn);

		$.ajax({
		    type: 'POST',
		    url: '/control/process/ajax-podcat.php',
		    data: {
		      nfn: fn
		    }
		  }).then(function(r_fn) {
		  	$("#podcat").html(r_fn);
		  	$('#ppo').selectpicker('refresh');
		});

	});
</script>


<?
	require_once H.'assets/tpl/footer.tpl';
?>