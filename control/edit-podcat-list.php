<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$get = filter($_GET['id']);
	$get_sql = mysql_fetch_assoc(mysql_query("SELECT * FROM `listovki_podcat` WHERE `id` = '$get' LIMIT 1"));
	$cat = mysql_fetch_assoc(mysql_query("SELECT * FROM `listovki_cat` WHERE `id` = '$get_sql[id_cat]' LIMIT 1"));

	$title_page = "Редактирование";

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li><a href="/control/all-listovki.php">Все магазины</a></li>';
	$arr_navig[] = '<li><a href="/control/listovka-podcat.php?id='.$cat['id'].'">'.$cat['name'].'</a></li>';
	$arr_navig[] = '<li>'.$get_sql['name'].'</li>';
	$arr_navig[] = '<li class="active">Редактирование</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>

			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        	<div class="col-lg-12">
								<div class="card-box">
									<form class="form-horizontal" role="form" method="post" action="/control/process/edit-podcat-list.php?id=<?=$get_sql['id'];?>&c=<?=$_GET['c'];?>" enctype="multipart/form-data">

										<div class="form-group">
											<label for="I1" class="col-sm-2 control-label">Название</label>
											<div class="col-sm-7">
												<input value='<?=$get_sql['name'];?>' type="text" id="I1" class="form-control" value="<?=$get_sql['name'];?>" placeholder="Название" name="name" required autocomplete="off">
											</div>
										</div>


										<div class="form-group">
											<label class="col-sm-2 control-label">Title</label>
											<div class="col-sm-6">
												<input value='<?=$get_sql['title'];?>' type="text" class="form-control" placeholder="Title" name="title" required autocomplete="off">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Description</label>
											<div class="col-sm-6">
												<input value='<?=$get_sql['desc'];?>' type="text" class="form-control" placeholder="Description" name="desc" required autocomplete="off">
											</div>
										</div>


										<div class="form-group">
	                                        <label class="col-md-2 control-label">Метки</label>
	                                        <div class="col-md-10">
	                                            <input value='<?=$get_sql['keys'];?>' type="text" name="keys_i" data-role="tagsinput" placeholder="Через запятую"/>
	                                        </div>
	                                    </div>

										<div class="form-group">
	                                        <label class="col-md-2 control-label">Фото</label>
	                                        <div class="col-md-6">
	                                        	<input type="file" class="filestyle" data-input="false" name="file">
	                                        </div>
	                                    </div>

										<?/*<div class="form-group">
											<label class="col-sm-2 control-label">Город</label>
											<div class="col-sm-2">
												<select name="city" class="selectpicker" data-style="btn-white">
													<option value="<?=$get_sql['city'];?>" selected>Не менять</option>
													<? $sql_opt = mysql_query("SELECT * FROM `cites` ORDER BY `id` DESC");
                            						while ($sql = mysql_fetch_assoc($sql_opt)) { ?>
                            							<option value="<?=$sql['id'];?>"><?=$sql['name'];?></option>
                            						<? } ?>
												</select>
											</div>
										</div>*/?>

										<div class="form-group">
											<label class="col-sm-2 control-label">Описание</label>
											<div class="col-sm-10">
												<textarea id="editor1" name="editor1"><?=$get_sql['content'];?></textarea>
												<script type="text/javascript">
													CKEDITOR.replace( 'editor1' );
												</script>
											</div>
										</div>
										



										<div class="form-group">
											<div class="col-sm-offset-4 col-sm-8">
												<button type="submit" class="btn btn-primary waves-effect waves-light">
													Сохранить
												</button>
												<a href="/control/all-listovki.php" class="btn btn-default waves-effect waves-light m-l-5">
													Отмена
												</a>
											</div>
										</div>
									</form>
								</div>
							</div>



                    </div> <!-- container -->
                               
                </div> <!-- content -->


<?
	require_once H.'assets/tpl/footer.tpl';
?>