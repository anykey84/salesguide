<?php
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$id = filter($_GET['id']);
    $option = mysql_fetch_assoc(mysql_query("SELECT * FROM `pages` WHERE `id` = '$_GET[id]' LIMIT 1"));
    if ($option['id'] == NULL) {
        header("location: /control/pages-all.php");
        exit;
    }

    $title_page = "Редактирование страницы";

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';
?>

 			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Редактировать страницу</h4>
                                <p class="text-muted page-title-alt">Панель администратора</p>
                            </div>
                        </div>

                        <div class="row">
                        	<div class="col-md-12">
                        		<div class="card-box">
								
								<form class="form-horizontal" role="form" action="/control/process/pages-edit.php?id=<?=$option['id'];?>" method="post" enctype="multipart/form-data"> 

                        			<div class="form-group">
	                                    <label class="col-md-2 control-label">Название страницы</label>
	                                    <div class="col-md-6">
	                                        <input type="text" name="name" value="<?=$option['name'];?>" class="form-control" placeholder="Название страницы" required>
	                                    </div>
	                                </div>

	                                

	                                

	                               	<div class="form-group">
	                                    <label class="col-md-2 control-label">Keywords</label>
	                                    <div class="col-md-10">
	                                        <input type="text" name="keys" value="<?=$option['keys'];?>" data-role="tagsinput" placeholder="Через запятую"/>
	                                    </div>
	                                </div>

	                                <div class="form-group">
	                                    <label class="col-md-2 control-label">Description</label>
	                                    <div class="col-md-6">
	                                        <input type="text" name="desc" value="<?=$option['desc'];?>" class="form-control" placeholder="Для поисковиков">
	                                    </div>
	                                </div>

                        		</div>
                        	</div>
                        </div>


                        <div class="row">
                        	<div class="col-sm-12">
	                        	<div class="card-box">

	                        	<div class="row">
	                        		<div class="col-md-12">
	                        			<textarea id="editor1" name="editor1"><?=$option['content'];?></textarea>
										<script type="text/javascript">
											CKEDITOR.replace( 'editor1' );
										</script>
	                        		</div>
	                        	</div>

								</div>
							</div>
						</div>

						<div class="row">
	                        <div class="col-sm-12">
	                        	<div class="card-box">
								
	                        	<button class="btn btn-success waves-effect waves-light" type="submit" id="fnb">
									Сохранить изменения
								</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

								</form>

								<a href="/control/pages-all.php" class="btn btn-warning waves-effect">Отмена</a>

								</div>
							</div>
						</div>



                    </div>
                </div>
            </div>

<?
	require_once H.'assets/tpl/footer.tpl';
?>