<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$get = filter($_GET['id']);
	$get_sql = mysql_fetch_assoc(mysql_query("SELECT * FROM `listovki_podcat` WHERE `id` = '$get' LIMIT 1"));
    $mag = mysql_fetch_assoc(mysql_query("SELECT * FROM `listovki_cat` WHERE `id` = '$get_sql[id_cat]' LIMIT 1"));

	$title_page = "Листовки. Магазин: ".$mag['name'];

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li><a href="/control/all-listovki.php">Листовки</a></li>';
    $arr_navig[] = '<li><a href="/control/listovka-podcat.php?id='.$mag['id'].'">'.$mag['name'].'</a></li>';
	$arr_navig[] = '<li class="active">'.$get_sql['name'].'</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>

			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                            	<div class="btn-group pull-right m-t-15">
                                    <a href="/control/new-photos-list.php?id=<?=$get_sql['id'];?>" class="btn btn-default dropdown-toggle waves-effect waves-light">Добавить фотографии</a>
                                </div>

                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="1">ID</th>
                                    <th>Фотограция</th>
                                    <th width="1" class="text-center">Действия</th>
                                </tr>
                                </thead>


                                <tbody>

                                <? $n=0; $sql_opt = mysql_query("SELECT * FROM `listovki_photos` WHERE `id_podcat` = '$get_sql[id]' ORDER BY `id` ASC");
                            	while ($sql = mysql_fetch_assoc($sql_opt)) { $n++;
                                ?>
	                                <tr>
                                        <td><?=$sql['id'];?></td>
	                                    <td><a target="_blank" href="/gallery/listovki-photo/<?=$sql['id_list'];?>-<?=$sql['id_podcat'];?>/<?=$sql['photo'];?>"><?=$sql['photo'];?></a></td>
	                                    <td class="text-center">
	                                    	<a href="javascript:;" onclick="del('<?=$sql['id'];?>');" class="btn btn-danger btn-custom btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Удалить"><i class="md md-delete"></i></a>
	                                    </td>
	                                </tr>
                            	<? } ?>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>




                    </div> <!-- container -->
                               
                </div> <!-- content -->

   	<script type="text/javascript">
    	$(document).ready(function () {
        	$('#datatable').dataTable();
        });

        function del(id) {
	        if (confirm('Подтверждаете удаление?')) {
	            location.href = '/control/process/del-ph-list.php?id='+id+'&s=<?=$get;?>';
	            return true;
	        } else {
	            return false;
	        }
    	}
    </script>

<?
	require_once H.'assets/tpl/footer.tpl';
?>