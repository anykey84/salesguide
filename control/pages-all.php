<?php
	require_once '../core/db.php';
    require_once H.'control/functions/fnc.php';

    $title_page = "Все страницы";

    $arr_navig[] = '<li><a href="/control">Главная</a></li>';
    $arr_navig[] = '<li>Все страницы</li>';

    require_once H.'assets/tpl/header.tpl';
    require_once H.'assets/tpl/left.tpl';

?>

 			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                            echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">


                            <div class="col-lg-8">
                                <h4 class="m-t-0 header-title"><b>Список всех созданных страниц</b></h4>

                                <div class="p-20">
                                    <table class="table table-bordered m-0">
                                                     
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Название</th>
                                                <th>Действие</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <?
                                        $numb = 0;
                                        $option_all = mysql_query("SELECT * FROM `pages` ORDER BY `id` DESC");
                                            while ($option = mysql_fetch_assoc($option_all)) {
                                                $numb++;
                                        ?>
                                                <tr>
                                                    <th scope="row"><?=$numb;?></th>
                                                    <td><?=$option['name'];?></td>
                                                    
                                                    <td>
                                                        <a href="/control/pages-edit.php?id=<?=$option['id'];?>">Редактировать</a>
                                                        /
                                                        <a href="javascript:void(0);" onclick="return confirmDelete(<?=$option['id'];?>);">Удалить</a>
                                                    </td>
                                                </tr>

                                        <? } ?>
    
                                         </tbody>
                                    </table>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>


<script type="text/javascript">
    function confirmDelete(id) {
        if (confirm("Вы подтверждаете удаление?")) {
            location.href = '/control/process/pages-del.php?id='+id;
            return true;
        } else {
            return false;
        }
    }
</script>

<?
    require_once H.'assets/tpl/footer.tpl';
?>