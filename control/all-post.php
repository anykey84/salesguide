<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$title_page = "Список всех акций";

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li class="active">Все акции</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>


<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                            	<div class="btn-group pull-right m-t-15">
                                    <a href="/control/new-post.php" class="btn btn-default dropdown-toggle waves-effect waves-light">Добавить акцию</a>
                                </div>

                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="1">ID</th>
                                    <th>Название</th>
                                    <th>Категория</th>
                                    <th>Подкатегория</th>
                                    <th>Город</th>
                                    <th>Дата добавления</th>
                                    <th width="1" class="text-center">Действия</th>
                                </tr>
                                </thead>


                                <tbody>

                                <? $n=0; $sql_opt = mysql_query("SELECT * FROM `akcii` ORDER BY `id` ASC");
                            	while ($sql = mysql_fetch_assoc($sql_opt)) { $n++;
                                    //$sub = mysql_result(mysql_query("SELECT COUNT(*) FROM `category_sub` WHERE `id_category` = '$sql[id]'"), 0);
                                    $ocat = mysql_fetch_assoc(mysql_query("SELECT * FROM `category` WHERE `id` = '$sql[id_cat]' LIMIT 1"));
                                    $opodcat = mysql_fetch_assoc(mysql_query("SELECT * FROM `category_sub` WHERE `id` = '$sql[id_podcat]' LIMIT 1"));
                                    $ocity = mysql_fetch_assoc(mysql_query("SELECT * FROM `cites` WHERE `id` = '$sql[id_city]' LIMIT 1"));
                                ?>
	                                <tr>
                                        <td><?=$sql['id'];?></td>
	                                    <td><?=$sql['name'];?></td>
	                                    <td><?=$ocat['name'];?></td>
                                        <td><?=$opodcat['name'];?></td>
                                        <td><?=$ocity['name'];?></td>
                                        <td><?=$sql['date'];?></td>
	                                    <td class="text-center">
	                                    	<a href="/control/edit-post.php?id=<?=$sql['id'];?>" class="btn btn-primary btn-custom btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Редактировать"><i class="md md-border-color"></i></a>&nbsp;&nbsp;&nbsp;
	                                    	<a href="javascript:;" onclick="del('<?=$sql['id'];?>');" class="btn btn-danger btn-custom btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Удалить"><i class="md md-delete"></i></a>
	                                    </td>
	                                </tr>
                            	<? } ?>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>




                    </div> <!-- container -->
                               
                </div> <!-- content -->

   	<script type="text/javascript">
    	$(document).ready(function () {
        	$('#datatable').dataTable();
        });

        function del(id) {
	        if (confirm('Подтверждаете удаление?')) {
	            location.href = '/control/process/del-akc.php?id='+id;
	            return true;
	        } else {
	            return false;
	        }
    	}
    </script>

<?
	require_once H.'assets/tpl/footer.tpl';
?>