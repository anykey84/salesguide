<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$title_page = "Добавление магазина";

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li><a href="/control/all-roz.php">Все розыгрыши</a></li>';
	$arr_navig[] = '<li class="active">Добавление</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>

			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        	<div class="col-lg-12">
								<div class="card-box">
									<form class="form-horizontal" role="form" method="post" action="/control/process/new-roz.php" enctype="multipart/form-data">
										<div class="form-group">
											<label for="I1" class="col-sm-2 control-label">Название</label>
											<div class="col-sm-6">
												<input type="text" id="I1" class="form-control" placeholder="Название" name="name" required autocomplete="off">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Title</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" placeholder="Title" name="title" required autocomplete="off">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Description</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" placeholder="Description" name="desc" required autocomplete="off">
											</div>
										</div>


										<div class="form-group">
	                                        <label class="col-md-2 control-label">Метки</label>
	                                        <div class="col-md-10">
	                                            <input type="text" name="keys_i" data-role="tagsinput" placeholder="Через запятую"/>
	                                        </div>
	                                    </div>

										<div class="form-group">
	                                        <label class="col-md-2 control-label">Фото</label>
	                                        <div class="col-md-6">
	                                        	<input type="file" class="filestyle" data-input="false" name="file" required>
	                                        </div>
	                                    </div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Город</label>
											<div class="col-sm-2">
												<select name="city" class="selectpicker" data-style="btn-white">
													<? $sql_opt = mysql_query("SELECT * FROM `cites` ORDER BY `id` DESC");
                            						while ($sql = mysql_fetch_assoc($sql_opt)) { ?>
                            							<option value="<?=$sql['id'];?>"><?=$sql['name'];?></option>
                            						<? } ?>
												</select>
											</div>
										</div>




										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" class="btn btn-primary waves-effect waves-light">
													Добавить
												</button>
												<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
													Отмена
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>



                    </div> <!-- container -->
                               
                </div> <!-- content -->


<script type="text/javascript">
	$("#cat").change(function() {
		//var fn = [$("#cat option:selected").val()];
		var fn = $('#cat').val();
		console.log(fn);

		$.ajax({
		    type: 'POST',
		    url: '/control/process/ajax-podcat.php',
		    data: {
		      nfn: fn
		    }
		  }).then(function(r_fn) {
		  	$("#podcat").html(r_fn);
		  	$('#ppo').selectpicker('refresh');
		});

	});
</script>


<?
	require_once H.'assets/tpl/footer.tpl';
?>