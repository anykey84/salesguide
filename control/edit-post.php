<?php
	
	require_once '../core/db.php';
	require_once H.'control/functions/fnc.php';

	$get = filter($_GET['id']);
	$get_sql = mysql_fetch_assoc(mysql_query("SELECT * FROM `akcii` WHERE `id` = '$get' LIMIT 1"));

	$title_page = "Редактирование акции";

	$arr_navig[] = '<li><a href="/control">Главная</a></li>';
	$arr_navig[] = '<li><a href="/control/all-post.php">Все акции</a></li>';
	$arr_navig[] = '<li class="active">Редактирование</li>';

	require_once H.'assets/tpl/header.tpl';
	require_once H.'assets/tpl/left.tpl';

?>

			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-header-2">
                                    <h4 class="page-title"><?=$title_page;?></h4>
                                    <ol class="breadcrumb">
                                        <?
                                        foreach ($arr_navig as $navig) {
                                        	echo $navig;
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        	<div class="col-lg-12">
								<div class="card-box">
									<form class="form-horizontal" role="form" method="post" action="/control/process/edit-akcia.php?id=<?=$get_sql['id'];?>" enctype="multipart/form-data">
										<div class="form-group">
											<label for="I1" class="col-sm-2 control-label">Заголовок</label>
											<div class="col-sm-6">
												<input value='<?=$get_sql['name'];?>' type="text" id="I1" class="form-control" placeholder="Название" name="name" required autocomplete="off">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Title</label>
											<div class="col-sm-6">
												<input value='<?=$get_sql['title'];?>' type="text" class="form-control" placeholder="Title" name="title" required autocomplete="off">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Description</label>
											<div class="col-sm-6">
												<input value='<?=$get_sql['desc'];?>' type="text" class="form-control" placeholder="Description" name="desc" required autocomplete="off">
											</div>
										</div>

										<?/*<div class="form-group">
											<label class="col-sm-2 control-label">Мета тег H1</label>
											<div class="col-sm-6">
												<input value="<?=$get_sql['h1'];?>" type="text" class="form-control" placeholder="H1" name="h1" required autocomplete="off">
											</div>
										</div>*/?>

										<div class="form-group">
	                                        <label class="col-md-2 control-label">Метки</label>
	                                        <div class="col-md-10">
	                                            <input value='<?=$get_sql['keys_i'];?>' type="text" name="keys_i" data-role="tagsinput" placeholder="Через запятую"/>
	                                        </div>
	                                    </div>

										<div class="form-group">
	                                        <label class="col-md-2 control-label">Фото</label>
	                                        <div class="col-md-6">
	                                        	<input type="file" class="filestyle" data-input="false" name="file">
	                                        </div>
	                                    </div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Город</label>
											<div class="col-sm-2">
												<select name="city" class="selectpicker" data-style="btn-white">
													<option value="<?=$get_sql['id_city'];?>" selected>Не менять</option>
													<? $sql_opt = mysql_query("SELECT * FROM `cites` ORDER BY `id` DESC");
                            						while ($sql = mysql_fetch_assoc($sql_opt)) { ?>
                            							<option value="<?=$sql['id'];?>"><?=$sql['name'];?></option>
                            						<? } ?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Категория</label>
											<div class="col-sm-3">
												<select name="cat" id="cat" class="selectpicker" data-style="btn-white">
													<option value="<?=$get_sql['id_cat'];?>" selected>Не менять</option>
													<? $sql_opt1 = mysql_query("SELECT * FROM `category` ORDER BY `name` ASC");
                            							while ($cat = mysql_fetch_assoc($sql_opt1)) {  ?>
                            							<option value="<?=$cat['id'];?>"><?=$cat['name'];?></option>
                            						<? } ?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Подкатегория</label>
											<div class="col-sm-3">
												<span id="podcat">
													<select name="podcat" class="selectpicker" data-style="btn-white" required="">
														<option value="<?=$get_sql['id_podcat'];?>" selected>Не менять</option>
														<option disabled>Выберите категорию</option>
													</select>
												</span>
											</div>
										</div>

										<hr><div class="form-group">
											<label class="col-sm-2 control-label">Действует с </label>
											<div class="col-sm-2">
												<input value="<?=$get_sql['ds'];?>" type="text" class="form-control" placeholder="день/мес/год" name="ds" required autocomplete="off">
											</div>
											<label class="col-sm-1 control-label">по</label>
											<div class="col-sm-2">
												<input value="<?=$get_sql['dp'];?>" type="text" class="form-control" placeholder="день/мес/год" name="dp" required autocomplete="off">
											</div>
											<div class="col-sm-2"><font color="red">* 0 - бессрочно</font></div>
										</div><hr>

										<div class="form-group">
											<label class="col-sm-2 control-label">Назв. организации</label>
											<div class="col-sm-6">
												<input value='<?=$get_sql['name_org'];?>' type="text" class="form-control" placeholder="Введите текст" name="name_org" required autocomplete="off">
											</div>
										</div><hr>

										<div class="form-group">
											<label class="col-sm-2 control-label">Адрес</label>
											<div class="col-sm-6">
												<input value='<?=$get_sql['adres_org'];?>' type="text" class="form-control" placeholder="Введите текст" name="adres_org" required autocomplete="off">
											</div>
										</div><hr>

										<div class="form-group">
											<label class="col-sm-2 control-label">Телефоны</label>
											<div class="col-sm-6">
												<input value="<?=$get_sql['tel_org'];?>" type="text" class="form-control" placeholder="Введите текст, напр: тел1, тел2, тел3" name="tel_org" required autocomplete="off">
											</div>
											<label class="col-sm-2">* через запятую</label>
										</div><hr>

										<div class="form-group">
											<label class="col-sm-2 control-label">Сайт</label>
											<div class="col-sm-4">
												<input value="<?=$get_sql['site_org'];?>" type="text" class="form-control" placeholder="Введите текст" name="site_org" required autocomplete="off">
											</div>
										</div><hr>

										<div class="form-group">
											<label class="col-sm-2 control-label">Соц. сети</label>
											<div class="col-sm-6">
												<input value="<?=$get_sql['soc_org'];?>" type="text" class="form-control" placeholder="Введите текст" name="soc_org" required autocomplete="off">
											</div>
											<label class="col-sm-2">* через запятую</label>
										</div><hr>

										<div class="form-group">
											<label class="col-sm-2 control-label">Описание</label>
											<div class="col-sm-10">
												<textarea id="editor1" name="editor1"><?=$get_sql['opis'];?></textarea>
												<script type="text/javascript">
													CKEDITOR.replace( 'editor1' );
												</script>
											</div>
										</div>




										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<button type="submit" class="btn btn-primary waves-effect waves-light">
													Сохранить акцию
												</button>
												<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
													Отмена
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>



                    </div> <!-- container -->
                               
                </div> <!-- content -->


<script type="text/javascript">
	$("#cat").change(function() {
		var fn = $("#cat option:selected").val();

		$.ajax({
		    type: 'POST',
		    url: '/control/process/ajax-podcat2.php',
		    data: {
		      nfn: fn
		    }
		  }).then(function(r_fn) {
		  	$("#podcat").html(r_fn);
		  	$('#ppo').selectpicker('refresh');
		});

	});
</script>


<?
	require_once H.'assets/tpl/footer.tpl';
?>