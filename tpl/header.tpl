<!DOCTYPE html>
<html lang="en-US" class="no-js no-svg">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!--<script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>!-->
		<title><?=$title;?></title>
		<meta name="description" content="<?=$desc;?>">
	    <meta name="keywords" content="<?=$keys;?>">

	    <link rel="icon" type="image/png" href="/style/images/favicon.png" />

	    <style type="text/css">
	    	body {
	    		text-transform: none;
	    	}

	    	.back {
    		    width: 100%;
			    background: rgb(0, 0, 0, 0.8);
			    height: 1000px;
			    z-index: 12;
			    position: fixed;
	    	}

	    	.front {
	    		z-index: 20;
			    background: white;
			    position: fixed;
			    width: 400px;
			    margin-left: 38%;
			    margin-top: 22%;
			    height: 300px;
	    	}

	    	.popup_dismiss {
			    position: absolute;
			    right: 20px;
			    bottom: 20px;
	    	}

	    </style>

		
		<link rel='stylesheet' id='live-search-css'  href='/style/css/dopstyle.css' type='text/css' media='all' />
		<link rel='stylesheet' id='live-search-css'  href='/style/css/live-search.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='contact-form-7-css'  href='/style/css/styles.css' type='text/css' media='all' />
		<link rel='stylesheet' id='animate-css-css'  href='/style/css/animate.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='magnific-popup-css'  href='/style/css/magnific-popup.css' type='text/css' media='all' />
		<link rel='stylesheet' id='bootstrap-css'  href='/style/css/bootstrap.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='font-awesome-css'  href='/style/css/font-awesome.min.css' type='text/css' media='all' />

		<link rel='stylesheet' id='pe-icon-7-stroke-css'  href='/style/css/pe-icon-7-stroke.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='slick-css'  href='/style/css/slick.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='themify-css'  href='/style/css/themify-icons.css' type='text/css' media='all' />
		<link rel='stylesheet' id='chosen-css'  href='/style/css/chosen.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='growl-css'  href='/style/css/jquery.growl.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='ovic-toolkit-css'  href='/style/css/mapper.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='ovic-rating-css'  href='/style/css/post-rating.css' type='text/css' media='all' />
		<link rel='stylesheet' id='rs-plugin-settings-css'  href='/style/css/settings.css' type='text/css' media='all' />

		<link rel='stylesheet' id='jquery-colorbox-css'  href='/style/css/colorbox.css' type='text/css' media='all' />
		<link rel='stylesheet' id='jquery-selectBox-css'  href='/style/css/jquery.selectBox.css' type='text/css' media='all' />
		<link rel='stylesheet' id='yith-wcwl-main-css'  href='/style/css/style.css' type='text/css' media='all' />
		<link rel='stylesheet' id='electronics-fonts-css'  href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i|Lato:300,300i,400,400i,700,700i,900,900i&#038;subset=latin,latin-ext' type='text/css' media='all' />
		<link rel='stylesheet' id='flaticon-css'  href='/style/css/flaticon.css' type='text/css' media='all' />
		<link rel='stylesheet' id='electronics_custom_css-css'  href='/style/css/style1.css' type='text/css' media='all' />
		
		<link rel='stylesheet' id='ovic-style-css'  href='/style/css/frontend.css' type='text/css' media='all' />

	
		<link rel='stylesheet' id='js_composer_front-css'  href='/style/css/js_composer.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='ovic-popup-css'  href='/style/css/popup.css' type='text/css' media='all' />

		<link rel='stylesheet' id='megamenu-frontend-css'  href='/style/css/megamenu-frontend.css' type='text/css' media='all' />

		

		<script src='/style/js/jquery.js'></script>

		<script defer src='/style/js/jquery-migrate.min.js'></script>
		<script defer src='/style/js/jquery.themepunch.tools.min.js'></script>
		<script defer src='/style/js/jquery.themepunch.revolution.min.js'></script>

		<script defer src='/style/js/add-to-cart.min.js'></script>
		<script defer src='/style/js/woocommerce-add-to-cart.js'></script>



		<link rel="alternate" type="application/json+oembed" href="/style/unk/1" />
		<link rel="alternate" type="text/xml+oembed" href="/style/unk/2" />

		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-38813232-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-38813232-2');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50310013 = new Ya.Metrika2({
                    id:50310013,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });
 
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";
 
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50310013" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

		
		<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/style/css/vc_lte_ie9.min.css" media="screen"><![endif]-->

		<script type="text/javascript">
			function setREVStartSize(e){									
				try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
				}catch(d){console.log("Failure at Presize of Slider:"+d)}						
			};
		</script>

	</head>

	<body class="home page-template page-template-templates page-template-fullwidth page-template-templatesfullwidth-php page page-id-1834 ovic-popup-on woocommerce-no-js woocommerce-wishlist woocommerce woocommerce-page Electronics-1.0.0 wpb-js-composer js-comp-ver-5.5.2 vc_responsive">

	<div style="display: none;" class="change_country_popup">
		<div class="back"></div>
		<div class="front">
			
			<h6>Выберите город из списка:</h6>
			<ul class="CountryList">
				<? $sql_optCatS = mysqli_query($mysqli, "SELECT * FROM `cites` ORDER BY `name` ASC");

				while ($ss = mysqli_fetch_assoc($sql_optCatS)) { 

					$active = "";	
					foreach (explode(", ", $_COOKIE['my_cityes']) as $city_name) {
						if ($city_name == $ss['id']) {
							$active = "ActiveCountry";
						}
					}
				?>
					<li><input class="<?php echo $active; ?>" type="checkbox" class="ChangeCountryCheck"><a data-id="<?= $ss['id'];?>" data-href="<?= $_SERVER['REQUEST_URI']; ?>"><?=$ss['name'];?></a></li>
				<?  } ?>

			</ul>


			<button class="popup_dismiss">close</button>

		</div>
	</div>

	<div class="header-sticky">
        <div class="container">
            <div class="header-nav-inner">					            
				<div data-items="11" class="vertical-wrapper block-nav-category has-vertical-menu <? if ($_SERVER['PHP_SELF'] == '/index.php') { ?> always-open <? } ?>">
					<div class="block-title">
						<span class="before">
							<span></span>
							<span></span>
							<span></span>
						</span>
						<span class="text-title">АКЦИИ И СКИДКИ</span>
					</div>
					<div class="block-content verticalmenu-content">
						<div class=" ovic-menu-wapper vertical">
							<ul id="menu-vertical-menu" class="ovic-nav vertical-menu ovic-menu">
								<? $sql_optCat = mysqli_query($mysqli, "SELECT * FROM `category` ORDER BY `name` ASC");
								while ($cat = mysqli_fetch_assoc($sql_optCat)) {
									$countQuery = mysqli_query($mysqli, "SELECT COUNT(*) FROM `akcii` WHERE `id_cat` = '$cat[id]'");
									$r1 = mysqli_fetch_row($countQuery)[0];
								?>
								<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-<?=$cat['id'];?>">
									<a target="_blank" href="/categoriya-akcii/<?=$cat['url'];?>"><?=$cat['name'];?> (<?=$r1;?>)</a>
									<ul class="sub-menu">
										<? $sql_optSub = mysqli_query($mysqli, "SELECT * FROM `category_sub` WHERE `id_category` = '$cat[id]' ORDER BY `name` ASC");

										while ($sub = mysqli_fetch_assoc($sql_optSub)) { 
											$countQuery = mysqli_query($mysqli, "SELECT COUNT(*) FROM `akcii` WHERE `id_cat` = '$cat[id]' AND `id_podcat` = '$sub[id]'");
											$r2 = mysqli_fetch_row($countQuery)[0];
										?>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-<?=$cat['id'];?>">
											<a target="_blank" href="/podcategoriya-akcii/<?=$sub['url'];?>"><?=$sub['name'];?> (<?=$r2;?>)</a></li>
										<? } ?>
									</ul>
								</li>
								<? } ?>
							</ul>
						</div>                        
					
						<div class="view-all-category">
                            <a target="_blank" href="#" data-closetext="Свернуть" data-alltext="Все категории" class="btn-view-all open-cate">Все категории</a>
                        </div>
					</div>
				</div>
				
				<div class="box-header-nav main-menu-wapper">
					<div class=" ovic-menu-wapper horizontal">
						<ul id="menu-primary-menu" class="clone-main-menu electronics-nav main-menu ovic-menu">
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/populyarnie-akcii">Популярные</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/page/Novosti-skidok">Новости скидок</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/page/Otzyvy">Отзывы</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/kontakti">Контакты</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/o-kompanii">О компании</a>
								</li>
							<!--<li id="menu-item-4071" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4071 menu-item-has-mega-menu menu-item-has-children item-megamenu"><a target="_blank" href="https://m-electronics.kute-themes.com/shop/">Shop</a>
									<div data-src="" style="width:900px;background-position: center;" class="lazy sub-menu megamenu" data-responsive="">
										<div class="vc_row wpb_row vc_row-fluid ovic_custom_5abd9894271b3 vc_custom_1522374799467">
											<div class="wpb_column vc_column_container vc_col-sm-3 ovic_custom_5abd9894271f5">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="ovic-custommenu vc_wp_custommenu wpb_content_element  ovic_custom_5abd989427232 ">
															<div class="widget widget_nav_menu"><h2 class="widgettitle">Product Features</h2>
																<div class=" ovic-menu-wapper horizontal">
																	<ul id="menu-megamenu-product-01" class="menu ovic-menu"><li id="menu-item-4062" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4062"><a target="_blank" href="https://m-electronics.kute-themes.com/product/ullamcorper-mobile-j7/">Product Simple</a></li>
																		<li id="menu-item-4063" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4063"><a target="_blank" href="https://m-electronics.kute-themes.com/product/laptop-galaxy-book/">Product Variable</a></li>
																		<li id="menu-item-4064" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4064"><a target="_blank" href="https://m-electronics.kute-themes.com/product/chenxi-clock-cu100/">Product Group</a></li>
																		<li id="menu-item-4065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4065"><a target="_blank" href="https://m-electronics.kute-themes.com/product/sollicitudin-molestie-ipad-8/">Product External/Affiliate</a></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>!-->
						</ul>
					</div>        
				</div>
            </div>
        </div>
    </div>
			
	<header id="header" class="header style1">
		<div class="header-top">
            <div class="container">
                <div class="header-top-inner">
					<div class=" ovic-menu-wapper horizontal">
						<ul id="menu-top-menu-first" class="electronics-nav top-bar-menu ovic-menu">
							<li id="menu-item-4072" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4072"><a target="_blank" href="javascript:;"><span class="icon icon-font fa fa-envelope-o"></span>saleguide.ru@gmail.com</a></li>
							<li id="menu-item-4073" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4073"><a target="_blank" href="javascript:;"><span class="icon icon-font fa fa-phone"></span>8-800-550-14-51</a></li>
						</ul>
					</div>
					<div class=" ovic-menu-wapper horizontal">
						<ul id="menu-top-menu-second" class="electronics-nav top-bar-menu right ovic-menu">
							<li id="menu-item-4075" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4075"><a class="rename" style="cursor: pointer;"><span class="icon icon-font fa fa-map-marker"></span><?=$omysite['name'];?></a></li> <!--target="_blank" href="/smenit-gorod"  -->
						</ul>
					</div>
				</div>
            </div>
        </div>
		
		<div class="header-middle">
            <div class="container">
                <div class="header-middle-inner">
                    <div class="logo">
						<a target="_blank" href="http://sale.guide">
							<img alt="" src="/style/images/saleguide1.png" class="_rw" style="width: 150px;">
						</a>                    
					</div>
					
                    <div class="header-control">
						<div class="block-search">
							<form role="search" method="get" action="/process/poisk.php" class="form-search block-search ovic-live-search-form">
				                <input type="hidden" name="post_type" value="product"/>
								<div class="category">
									<select  name='product_cat' id='1451856388' class='category-search-option'  tabindex="1">
										<option value='0'>По всем категориям</option>
										<? $sql_optCat = mysqli_query($mysqli, "SELECT * FROM `category` ORDER BY `name` ASC");
										while ($cat = mysqli_fetch_assoc($sql_optCat)) { ?>
											<option class="level-0" value="<?=$cat['id'];?>_glav"><?=$cat['name'];?></option>
											<? $sql_optSub = mysqli_query($mysqli, "SELECT * FROM `category_sub` WHERE `id_category` = '$cat[id]' ORDER BY `name` ASC");
											while ($sub = mysqli_fetch_assoc($sql_optSub)) { ?>
												<option class="level-1" value="<?=$sub['id'];?>_pod"><?=$sub['name'];?></option>
											<? } ?>
										<? } ?>
									</select>
								</div>
								
				                <div class="form-content search-box results-search">
									<div class="inner">
										<input autocomplete="off" type="text" class="searchfield txt-livesearch input" name="s" value="" placeholder="Что будем искать?" required>
									</div>
								</div>
								
								<button type="submit" class="btn-submit">
									<span class="fa fa-search" aria-hidden="true"></span>
								</button>
							</form>
						</div>
		                
							        
						<div class="block-minicart ovic-mini-cart ovic-dropdown">
							<div class="shopcart-dropdown block-cart-link" data-ovic="ovic-dropdown">
								<a target="_blank" class="link-dropdown" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Прямо сейчас пользователей на сайте">
									<span class="fa fa-users icon">
										<span class="count"><?=rand(285, 310);?></span>
									</span>
								</a>
							</div>      
						</div>
						
						
		                <div class="block-menu-bar">
                            <a target="_blank" class="menu-bar menu-toggle" href="javascript:;">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		
		
		<div class="header-mobile">
            <div class="container">
                <div class="header-mobile-inner">
                    <div class="logo">
						<a target="_blank" href="http://sale.guide">
							<img alt="" style="width: 150px;" src="/style/images/saleguide1.png" class="_rw" />
						</a>                    
					</div>
                    <div class="header-control">
                        <div class="header-settings ovic-dropdown">
                            <a target="_blank" href="#" data-ovic="ovic-dropdown">
                                <span class="fa fa-search" aria-hidden="true"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="search">
                                    <h4 class="title">Поиск:</h4>
									    <div class="block-search">
											<form role="search" method="get" action="/process/poisk.php" class="form-search block-search ovic-live-search-form">
												<div class="category">
													<select  name='product_cat' id='1067540064' class='category-search-option'  tabindex="1">
														<option value='0'>По всем категориям</option>
														<? $sql_optCat = mysqli_query($mysqli, "SELECT * FROM `category` ORDER BY `name` ASC");
														while ($cat = mysqli_fetch_assoc($sql_optCat)) { ?>
															<option class="level-0" value="<?=$cat['id'];?>_glav"><?=$cat['name'];?></option>
															<? $sql_optSub = mysqli_query($mysqli, "SELECT * FROM `category_sub` WHERE `id_category` = '$cat[id]' ORDER BY `name` ASC");
															while ($sub = mysqli_fetch_assoc($sql_optSub)) { ?>
																<option class="level-1" value="<?=$sub['id'];?>_pod"><?=$sub['name'];?></option>
															<? } ?>
														<? } ?>
													</select>
												</div>
												
												<div class="form-content search-box results-search">
													<div class="inner">
														<input autocomplete="off" type="text" class="searchfield txt-livesearch input" name="s" value="" placeholder="Что будем искать?" required>
													</div>
												</div>
												
												<button type="submit" class="btn-submit">
													<span class="fa fa-search" aria-hidden="true"></span>
												</button>
											</form>
										</div>
		                        </li>
							</ul>
                        </div>
						
                        <div class="block-menu-bar">
                            <a target="_blank" class="menu-bar menu-toggle" href="javascript:;">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
		        
				
		<div class="header-nav">
            <div class="container">
                <div class="header-nav-inner">
					<div data-items="11" class="vertical-wrapper block-nav-category has-vertical-menu <? if ($_SERVER['PHP_SELF'] == '/index.php') { ?> always-open <? } ?>">
						<div class="block-title">
							<span class="before">
								<span></span>
								<span></span>
								<span></span>
							</span>
							<span class="text-title">АКЦИИ И СКИДКИ</span>
						</div>
						<div class="block-content verticalmenu-content">
							<div class=" ovic-menu-wapper vertical support-mobile-menu">
								<ul id="menu-vertical-menu-1" class="ovic-nav vertical-menu ovic-menu ovic-clone-mobile-menu">
									<? $sql_optCat = mysqli_query($mysqli, "SELECT * FROM `category` ORDER BY `name` ASC");
									while ($cat = mysqli_fetch_assoc($sql_optCat)) { 
										$countQuery = mysqli_query($mysqli, "SELECT COUNT(*) FROM `akcii` WHERE `id_cat` = '$cat[id]'");
										$r1 = mysqli_fetch_row($countQuery)[0];
									?>
									<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-<?=$cat['id'];?>">
										<a target="_blank" href="/categoriya-akcii/<?=$cat['url'];?>"><?=$cat['name'];?> (<?=$r1;?>)</a>
										<ul class="sub-menu">
											<? $sql_optSub = mysqli_query($mysqli, "SELECT * FROM `category_sub` WHERE `id_category` = '$cat[id]' ORDER BY `name` ASC");
											while ($sub = mysqli_fetch_assoc($sql_optSub)) { 
												$countQuery = mysqli_query($mysqli, "SELECT COUNT(*) FROM `akcii` WHERE `id_cat` = '$cat[id]' AND `id_podcat` = '$sub[id]'");
												$r2 =  mysqli_fetch_row($countQuery)[0];
											?>
											<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-<?=$cat['id'];?>">
												<a target="_blank" href="/podcategoriya-akcii/<?=$sub['url'];?>"><?=$sub['name'];?> (<?=$r2;?>)</a></li>
											<? } ?>
										</ul>
									</li>
									<? } ?>
								</ul>
							</div>   
					
							<div class="view-all-category">
								<a target="_blank" href="javascript:;" data-closetext="Свернуть" data-alltext="Все категории" class="btn-view-all open-cate">Все категории</a>
							</div>
					    </div>
					</div>
					
		            <div class="box-header-nav">
						<div class=" ovic-menu-wapper horizontal support-mobile-menu">
							<ul id="menu-primary-menu-1" class="clone-main-menu electronics-nav main-menu ovic-menu ovic-clone-mobile-menu">
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/populyarnie-akcii">Популярные</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/page/Novosti-skidok">Новости скидок</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/page/Otzyvy">Отзывы</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/kontakti">Контакты</a>
								</li>
								<li id="menu-item-4170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1834 current_page_item menu-item-4170">
									<a target="_blank" href="/o-kompanii">О компании</a>
								</li>
								
								<!--<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4071 menu-item-has-mega-menu menu-item-has-children item-megamenu"><a target="_blank" href="https://m-electronics.kute-themes.com/shop/">Shop</a><div data-src="" style="width:900px;background-position: center;" class="lazy sub-menu megamenu" data-responsive=""><div class="vc_row wpb_row vc_row-fluid ovic_custom_5abd9894271b3 vc_custom_1522374799467">
								<div class="wpb_column vc_column_container vc_col-sm-3 ovic_custom_5abd9894271f5"><div class="vc_column-inner ">
								<div class="wpb_wrapper">            
								<div class="ovic-custommenu vc_wp_custommenu wpb_content_element  ovic_custom_5abd989427232 ">
										<div class="widget widget_nav_menu"><h2 class="widgettitle">Product Features</h2><div class=" ovic-menu-wapper horizontal"><ul id="menu-megamenu-product-2" class="menu ovic-menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4062"><a target="_blank" href="https://m-electronics.kute-themes.com/product/ullamcorper-mobile-j7/">Product Simple</a></li>
								<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4063"><a target="_blank" href="https://m-electronics.kute-themes.com/product/laptop-galaxy-book/">Product Variable</a></li>
								<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4064"><a target="_blank" href="https://m-electronics.kute-themes.com/product/chenxi-clock-cu100/">Product Group</a></li>
								<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4065"><a target="_blank" href="https://m-electronics.kute-themes.com/product/sollicitudin-molestie-ipad-8/">Product External/Affiliate</a></li>
								</ul></div></div>            </div>
								</div></div></div></div></div></li>!-->
							</ul>
						</div>                    
					</div>
                </div>
            </div>
        </div>
	</header>

	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".ActiveCountry").prop("checked", "checked")
			//console.log("aa");
			jQuery(".rename").click(function(){
				jQuery(".change_country_popup").show();
			});

			jQuery(".popup_dismiss").click(function(){
				jQuery(".change_country_popup").hide();
			});

			jQuery(".popup_dismiss").click(function(){
				result = [];

				for(let i = 0;  i < jQuery(".CountryList").children().length; i++) {
					if (jQuery(".CountryList").children().eq(i).children().eq(0).prop("checked")) {
						result.push(jQuery(".CountryList").children().eq(i).children().eq(1).attr("data-id"))
					}
				}
				
				result = result.join(", ");

				jQuery.post( "../process/ccity.php", { result: result })
				  .done(function(data) {
				    location.reload();

				  });
			});
		});

	</script>