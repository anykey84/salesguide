<? 
	if ($_SERVER['PHP_SELF'] != '/index.php' && $_SERVER['PHP_SELF'] != '/modules/akcii.php') {
		require_once H.'tpl/rek.tpl';
	}
?>

<footer class="footer style1 " >
	            <div class="iconbox-footer">
                <div class="container">
                    <div class="footer-inner">
                        <div class="row">
							                                <div class="ovic-iconbox col-lg-3 col-md-6 col-sm-6">
                                    <div class="iconbox-inner">
										                                            
										                                        <div class="content">
											                                                <h4 class="title"><a target="_blank" target="_blank" href="/saletv">SaleTV</a></h4>
																						                                                
											                                        </div>
                                    </div>
                                </div>
							                                <div class="ovic-iconbox col-lg-3 col-md-6 col-sm-6">
                                    <div class="iconbox-inner">
										                                            
										                                        <div class="content">
											                                                <h4 class="title"><a target="_blank" target="_blank" href="/fotogid">Фотогид</a></h4>
																						                                                
											                                        </div>
                                    </div>
                                </div>
							                                <div class="ovic-iconbox col-lg-3 col-md-6 col-sm-6">
                                    <div class="iconbox-inner">
										                                            
										                                        <div class="content">
											                                                <h4 class="title"><a target="_blank" target="_blank" href="/listovki">Каталоги</a></h4>
																						                                                
											                                        </div>
                                    </div>
                                </div>
							                                <div class="ovic-iconbox col-lg-3 col-md-6 col-sm-6">
                                    <div class="iconbox-inner">
										                                            
										                                        <div class="content">
											                                                <h4 class="title"><a target="_blank" target="_blank" href="/rozygryshi">Розыгрыши</a></h4>
																						                                                
											                                        </div>
                                    </div>
                                </div>
							                        </div>
                    </div>
                </div>
            </div>
		            <div class="top-footer">
                <div class="container">
                    <div class="footer-inner">
                        <div class="row">
							                                    <div class="col-lg-3 col-md-3 col-sm-12">
										<div id="media_image-3" class="widget widget_media_image"><a target="_blank" href="/">
											<img alt="" src="/style/images/saleguide1.png" class="_rw" style="width: 150px;">
										</a></div><div id="widget_ovic_iconbox-1" class="widget widget-ovic-iconbox">            <div class="iconbox-inner">
				                    <div class="icon"><span class="fa fa-location-arrow"></span></div>
				                <div class="content">
					                        <p class="text">350000. Краснодар.<br>Ростовское шоссе 14Б<br>пн-пт с 9-00 до 18-00</p>
					                </div>
            </div>
			</div><div id="widget_ovic_iconbox-2" class="widget widget-ovic-iconbox">            <div class="iconbox-inner">
				                    <div class="icon"><span class="fa fa-phone"></span></div>
				                <div class="content">
					                        <p class="text">Тел: 8-800-550-14-51</p>
					                </div>
            </div>
			</div><div id="widget_ovic_socials-1" class="widget widget-ovic-socials">            <div class="ovic-socials widget-socials  ">
                <div class="content-socials">
					                        <ul class="socials-list">

					                        	<li>
                                        <a target="_blank" target="_blank" href="https://vk.com/sale.guide">
                                            <span class="fa fa-vk" style="color: #4a76a8;"></span>
											VK                                        </a>
                                    </li>

															                                    
																							                                    <li>
                                        <a target="_blank" target="_blank" href="https://www.facebook.com/SaleGuide-321722558393355/?modal=admin_todo_tour">
                                            <span class="fa fa-facebook" style="color: #1a0dab;"></span>
											Facebook                                        </a>
                                    </li>
																							                                    <li>
                                        <a target="_blank" target="_blank" href="https://www.instagram.com/sale.guide/">
                                            <span class="fa fa-instagram" style="color: #B3348D;"></span>
											Instagram                                        </a>
                                    </li>
																							                                    
																							                                    <li>
                                        <a target="_blank" target="_blank" href="https://www.youtube.com/channel/UCW5XIK5reQMr4G3DTVsroXQ?view_as=subscriber">
                                            <span class="fa fa-youtube" style="color: #F8251C;"></span>
											YouTube                                        </a>
                                    </li>
															                        </ul>
					                </div>
            </div>
			</div>                                    </div>
															                                    <div class="col-lg-2 col-md-2 col-sm-4">
										<div id="ovic_nav_menu-5" class="widget widget_ovic_nav_menu"><h2 class="widgettitle">Компания<span class="arow"></span></h2><div class=" ovic-menu-wapper horizontal"><ul id="menu-our-store" class="menu ovic-menu"><li id="menu-item-3822" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3822"><a target="_blank" href="/vakansii">Вакансии</a></li>
<li id="menu-item-3823" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3823"><a target="_blank" href="/page/Reklamodateljam">Рекламодателям</a></li>
<li id="menu-item-3824" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3824"><a target="_blank" href="/page/Oferta">Оферта</a></li>
<li id="menu-item-3825" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3825"><a target="_blank" href="/kontakti">Контакты</a></li>
</ul></div></div>                                    </div>
															                                    <div class="col-lg-2 col-md-2 col-sm-4">
										<div id="ovic_nav_menu-6" class="widget widget_ovic_nav_menu"><h2 class="widgettitle">Сервис<span class="arow"></span></h2><div class=" ovic-menu-wapper horizontal"><ul id="menu-customer" class="menu ovic-menu"><li id="menu-item-3829" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3829"><a target="_blank" href="/page/Pravila-razmeschenija-kommentariev">Правила размещения комментариев</a></li>
<li id="menu-item-3830" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3830"><a target="_blank" href="/page/Sposoby-oplaty">Способы оплаты</a></li>
<li id="menu-item-3831" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3831"><a target="_blank" href="/page/Bonusy">Бонусы</a></li>
</ul></div></div>                                    </div>
															                                    <div class="col-lg-2 col-md-2 col-sm-4">
										<div id="ovic_nav_menu-7" class="widget widget_ovic_nav_menu"><h2 class="widgettitle">Дополнительно<span class="arow"></span></h2><div class=" ovic-menu-wapper horizontal"><ul id="menu-information" class="menu ovic-menu"><li id="menu-item-4044" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4044"><a target="_blank" href="/page/Kak-poluchit-skidku">Как получить скидку</a></li>
<li id="menu-item-4045" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4045"><a target="_blank" href="/page/Otzyvy">Отзывы</a></li>
</ul></div></div>                                    </div>
															                                    <div class="col-lg-3 col-md-3 col-sm-12">
										<div id="widget_ovic_mailchimp-2" class="widget widget-ovic-mailchimp"><h2 class="widgettitle">Подписка<span class="arow"></span></h2>            <div class="widget-form-wrap" style="background-image: url(https://m-electronics.kute-themes.com/wp-content/uploads/2018/01/newsletter-1.png)">
				<p class="desc">Получайте первым актуальные скидки и акции</p>				            <div class="newsletter-form-wrap">
								                <label class="text-field field-email">
                    <input class="input-text email email-newsletter" type="email" name="email"
                           placeholder="Ваш email адрес...">
                </label>
                <a target="_blank" href="#"
                   class="button btn-submit submit-newsletter">Подписаться</a>
            </div>
			            </div>
			</div>                                    </div>
															                        </div>
                    </div>
                </div>
            </div>
		            <div class="middle-footer">
                <div class="container">
                    <div class="footer-inner">
						<div id="media_gallery-1" class="widget widget_media_gallery"><h2 class="widgettitle">Мы принимаем к оплате:<span class="arow"></span></h2>
							<div id='gallery-1' class='gallery galleryid-1834 gallery-columns-1 gallery-size-full'>
								<figure class='gallery-item'>
			<div class='gallery-icon landscape'>
				<a target="_blank" href='/'><img width="51" height="36" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20viewBox%3D%270%200%2051%2036%27%2F%3E" class="attachment-full size-full lazy" alt="" data-src="https://m-electronics.kute-themes.com/wp-content/uploads/2018/01/02_HomeElectronic-3-1-1.jpg" /></a>
			</div></figure><figure class='gallery-item'>
			<div class='gallery-icon landscape'>
				<a target="_blank" href='/'><img width="52" height="36" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20viewBox%3D%270%200%2052%2036%27%2F%3E" class="attachment-full size-full lazy" alt="" data-src="https://m-electronics.kute-themes.com/wp-content/uploads/2018/01/03_HomeElectronic-1-1-1.jpg" /></a>
			</div></figure>

			<figure class='gallery-item'>
				<div class='gallery-icon landscape'>
					<a target="_blank" href='/'><img width="52" height="36" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20viewBox%3D%270%200%2052%2036%27%2F%3E" class="attachment-full size-full lazy" alt="" data-src="https://m-electronics.kute-themes.com/wp-content/uploads/2018/01/05_HomeElectronic-1-1.jpg" /></a>
				</div>
			</figure>

			<figure class='gallery-item'>
				<div class='gallery-icon landscape'>
					<a target="_blank" href='/'><img width="52" height="36" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20viewBox%3D%270%200%2052%2036%27%2F%3E" class="attachment-full size-full lazy" alt="" data-src="http://купить-турник.рф/images/pay1.jpg" /></a>
				</div>
			</figure>

			<figure class='gallery-item'>
				<div class='gallery-icon landscape'>
					<a target="_blank" href='/'><img width="52" height="36" src="data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20viewBox%3D%270%200%2052%2036%27%2F%3E" class="attachment-full size-full lazy" alt="" data-src="https://corp.qiwi.com/dam/jcr:75d303a3-c579-4fb4-be4c-258da7f2a05f/qiwi_logo_rgb_small.png" /></a>
				</div>
			</figure>


		</div>
</div>                    </div>
                </div>
            </div>
		            <div class="bottom-footer">
                <div class="container">
                    <div class="footer-inner">
                        <div class="row">
							                                <div class="col-md-6">
									<div id="text-2" class="widget widget_text">			<div class="textwidget"><p>© 2018 <strong>sale.guide</strong> Все права защищены</p>
</div>
		</div>                                </div>
														                                <div class="col-md-6">
									<div id="text-3" class="widget widget_text">			<div class="textwidget"><p><a target="_blank" href="#">Акции и скидки</a><span style="padding: 0 10px; font-size: 10px; vertical-align: bottom; color: rgba(0,0,0,0.3);">|</span><a target="_blank" href="/page/Otzyvy">Отзывы</a><span style="padding: 0 10px; font-size: 10px; vertical-align: bottom; color: rgba(0,0,0,0.3);">|</span><a target="_blank" href="/kontakti">Контакты</a><span style="padding: 0 10px; font-size: 10px; vertical-align: bottom; color: rgba(0,0,0,0.3);">|</span><a target="_blank" href="/o-kompanii">О компании</a></p>
</div>
		</div>                                </div>
							                        </div>
                    </div>
                </div>
            </div>
		        <?/*<div class="mobile-footer is-sticky">
            <div class="mobile-footer-inner">
                <div class="mobile-block">
                    <a target="_blank" href="/">
                        <span class="fa fa-home icon" aria-hidden="true"></span>
                        <span class="text">Гланая</span>
                    </a>
                </div>
				                    <div class="mobile-block mobile-block-compare">
                        <a target="_blank" href="/"
                           class="compare added" rel="nofollow">
                            <span class="fa fa-bar-chart icon"></span>
                            <span class="text">Compare</span>
                        </a>
                    </div>
								                        <div class="mobile-block mobile-block-wishlist">
                            <a target="_blank" class="woo-wishlist-link" href="/">
                            <span class="fa fa-heart icon">
                                <span class="count">0</span>
                            </span>
                                <span class="text">Wishlist</span>
                            </a>
                        </div>
									                    <div class="mobile-block mobile-block-minicart">
                        <a target="_blank" class="link-dropdown" href="/">
                        <span class="fa fa-shopping-bag icon">
                            <span class="count">0</span>
                        </span>
                            <span class="text">Cart</span>
                        </a>
                    </div>
				                <div class="mobile-block mobile-block-userlink">
                    <a target="_blank" data-ovic="ovic-dropdown" class="woo-wishlist-link"
                       href="/">
                        <span class="fa fa-user icon" aria-hidden="true"></span>
                        <span class="text">Account</span>
                    </a>
                </div>
                <div class="mobile-block block-menu-bar">
                    <a target="_blank" href="#" class="menu-bar menu-toggle">
                    <span class="icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                        <span class="text">Меню</span>
                    </a>
                </div>
            </div>
        </div>*/?>
		</footer>


<a href="#" class="backtotop">
    <i class="fa fa-angle-up"></i>
</a>



<script type="text/javascript">
	/*window.captureEvents(Event.CLICK);
	window.onclick = handle;

	function handle(e) {
       e.preventDefault();
       //window.routeEvent(e);
       var url = e.target.href;
       
       if (url == false) {	
       	console.log(url);
       }
       return true;
    }*/
</script>





<script type="text/template" id="tmpl-variation-template">
	<div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
	<div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
	<div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
	<p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<link rel='stylesheet' id='product-attributes-swatches-css'  href='/style/css/product-attribute.css' type='text/css' media='all' />
<link rel='stylesheet' id='photoswipe-css'  href='/style/css/photoswipe.css' type='text/css' media='all' />
<link rel='stylesheet' id='photoswipe-default-skin-css'  href='/style/css/default-skin.css' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='/style/css/prettyPhoto.css' type='text/css' media='all' />

<script defer src='/style/js/live-search.min.js'></script>
<script defer src='//maps.googleapis.com/maps/api/js?key=AIzaSyBMYcnNHYB7x1wJM7RO1Ia_ErYc4dQQC-s'></script>
<script defer src='/style/js/threesixty.min.js'></script>
<script defer src='/style/js/serialize-object.min.js'></script>
<script defer src='/style/js/jquery.magnific-popup.min.js'></script>
<script defer src='/style/js/bootstrap.min.js'></script>
<script defer src='/style/js/slick.min.js'></script>
<script defer src='/style/js/countdown.min.js'></script>
<script defer src='/style/js/chosen.min.js'></script>
<script defer src='/style/js/lazyload.min.js'></script>
<script defer src='/style/js/growl.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ovic_ajax_frontend = {"ajaxurl":"\/wp-admin\/admin-ajax.php","ovic_ajax_url":"\/?ovic-ajax=%%endpoint%%","security":"597b102ff3","added_to_cart_notification_text":"has been added to cart!","view_cart_notification_text":"View Cart","added_to_cart_text":"Product has been added to cart!","wc_cart_url":"https:\/\/m-electronics.kute-themes.com\/cart\/","added_to_wishlist_text":"Product added!","wishlist_url":"https:\/\/m-electronics.kute-themes.com\/","browse_wishlist_text":"Browse Wishlist","growl_notice_text":"Notice!","growl_duration":"6000","removed_cart_text":"Product Removed","wp_nonce_url":"https:\/\/m-electronics.kute-themes.com\/cart\/?_wpnonce=29cb1f73dd","data_slick":"{\"slidesMargin\":10,\"infinite\":false,\"slidesToShow\":4,\"focusOnSelect\":true}","data_responsive":"[{\"breakpoint\":1500,\"settings\":{\"slidesToShow\":4}},{\"breakpoint\":1200,\"settings\":{\"slidesToShow\":4}},{\"breakpoint\":992,\"settings\":{\"slidesToShow\":4}},{\"breakpoint\":768,\"settings\":{\"slidesToShow\":4}},{\"breakpoint\":480,\"settings\":{\"slidesToShow\":3}}]","single_add_to_cart":""};
/* ]]> */
</script>
<script defer src='https://m-electronics.kute-themes.com/wp-content/plugins/ovic-toolkit/includes/frontend/assets/js/frontend.min.js'></script>
<script defer src='https://m-electronics.kute-themes.com/wp-content/plugins/ovic-toolkit//includes/extends/mapper/assets/js/mapper.min.js'></script>
<script defer src='https://m-electronics.kute-themes.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script defer src='https://m-electronics.kute-themes.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script defer src='/style/js/woocommerce.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_f1de495b5c1d49d29d8c9857871e1762","fragment_name":"wc_fragments_f1de495b5c1d49d29d8c9857871e1762"};
/* ]]> */
</script>
<script defer src='/style/js/cart-fragments.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_woocompare = {"ajaxurl":"\/?wc-ajax=%%endpoint%%","actionadd":"yith-woocompare-add-product","actionremove":"yith-woocompare-remove-product","actionview":"yith-woocompare-view-table","actionreload":"yith-woocompare-reload-product","added_label":"Added","table_title":"Product Comparison","auto_open":"yes","loader":"https:\/\/m-electronics.kute-themes.com\/wp-content\/plugins\/yith-woocommerce-compare\/assets\/images\/loader.gif","button_text":"Compare","cookie_name":"yith_woocompare_list","close_label":"Close"};
/* ]]> */
</script>
<script defer src='/style/js/woocompare.min.js'></script>
<script defer src='/style/js/jquery.colorbox-min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_qv = {"ajaxurl":"\/wp-admin\/admin-ajax.php","loader":"https:\/\/m-electronics.kute-themes.com\/wp-content\/plugins\/yith-woocommerce-quick-view\/assets\/image\/qv-loader.gif","is2_2":"","lang":""};
/* ]]> */
</script>
<script defer src='/style/js/frontend.min.js'></script>
<script defer src='/style/js/jquery.prettyPhoto.min.js'></script>
<script defer src='/style/js/jquery.selectBox.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_wcwl_l10n = {"ajax_url":"\/wp-admin\/admin-ajax.php","redirect_to_cart":"no","multi_wishlist":"","hide_add_button":"1","is_user_logged_in":"","ajax_loader_url":"https:\/\/m-electronics.kute-themes.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif","remove_from_wishlist_after_add_to_cart":"yes","labels":{"cookie_disabled":"We are sorry, but this feature is available only if cookies are enabled on your browser.","added_to_cart_message":"<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"},"actions":{"add_to_wishlist_action":"add_to_wishlist","remove_from_wishlist_action":"remove_from_wishlist","move_to_another_wishlist_action":"move_to_another_wishlsit","reload_wishlist_and_adding_elem_action":"reload_wishlist_and_adding_elem"}};
/* ]]> */
</script>
<script defer src='/style/js/jquery.yith-wcwl.js'></script>
<script defer src='/style/js/comment-reply.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var electronics_ajax_frontend = {"ajaxurl":"https:\/\/m-electronics.kute-themes.com\/wp-admin\/admin-ajax.php","security":"c38b1e59fe"};
var electronics_global_frontend = {"ovic_sticky_menu":"1"};
/* ]]> */
</script>
<script defer src='/style/js/functions.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ovic_popup = {"ajaxurl":"https:\/\/m-electronics.kute-themes.com\/wp-admin\/admin-ajax.php","security":"0be99bcbda","enable_popup":"on","delay_time":"10000","enable_popup_mobile":"0","pages_display":["1834"],"current_page_id":"1834"};
/* ]]> */
</script>
<script defer src='/style/js/popup.min.js'></script>
<script defer src='/style/js/jquery.actual.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ovic_megamenu_frontend = {"title":"MAIN MENU"};
/* ]]> */
</script>
<script defer src='/style/js/megamenu-frontend.js'></script>
<script defer src='/style/js/wp-embed.min.js'></script>
<script defer src='/style/js/js_composer_front.min.js'></script>
<script defer src='/style/js/underscore.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script defer src='/style/js/wp-util.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"Sorry, no products matched your selection. Please choose a different combination.","i18n_make_a_selection_text":"Please select some product options before adding this product to your cart.","i18n_unavailable_text":"Sorry, this product is unavailable. Please choose a different combination."};
/* ]]> */
</script>
<script defer src='/style/js/add-to-cart-variation.min.js'></script>
<script defer src='/style/js/product-attribute.js'></script>

<script defer src='/style/js/mailchimp.min.js'></script>
<script defer src='/style/js/jquery.zoom.min.js'></script>
<script defer src='/style/js/photoswipe.min.js'></script>
<script defer src='/style/js/photoswipe-ui-default.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"1","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":"1"};
/* ]]> */
</script>
<script defer src='/style/js/single-product.min.js'></script>
<script defer src='https://m-electronics.kute-themes.com/wp-content/plugins/js_composer/assets/lib/vc_tabs/vc-tabs.min.js'></script>



<script>
/*(function() {
 
var refs=document.getElementsByTagName("A")
for (var i=0, L=refs.length; i<L; i++) {
    refs[i].onclick=function() {
        //alert(this.href) //this.href - адрес ссылки
        window.open(this.href, "_blank");
    }
}
 
})()*/

	

</script>



<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'laWkqyAxcU';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->


</body>
</html>