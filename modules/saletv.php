<?php
	require_once '../core/db.php';	
	require_once '../process/library-parser.php';
	$html = file_get_html('https://www.youtube.com/channel/UCW5XIK5reQMr4G3DTVsroXQ/videos');
	$videos = $html->find('#channels-browse-content-grid li');

	$title = 'SaleTV - SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве';
	$desc = "SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве";
	$keys = "#скидки, #акции, #распродажи, #купоны, #промокоды, #скидкимосква, #скидкикраснодар, #акциикраснодар, #акциимосква, #распродажикраснодар, #распродажимосква";


	require_once '../tpl/header.tpl';
?>


<div class="single-thumb-horizontal main-container shop-page left-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="woocommerce-breadcrumb breadcrumb">
					<li><a target="_blank" href="/">Главная</a></li>
					<li>SaleTV</li>
				</ul>
			</div>

			<div class="main-content col-lg-12 col-md-12 has-sidebar">

				<div class="row auto-clear equal-container better-height ovic-products  style-1"><ul class="products columns-4" style="padding-bottom: 0px; margin-bottom: 0px; padding-top: 0px; margin-top: 0px;">
					
					<? foreach ($videos as $key => $value) {
						$link = $value->find('.yt-lockup-title a');
						foreach ($link as $key2 => $value2) { $codeVideo = str_replace('/watch?v=', '', $value2->href); $nameV = $value2->plaintext; ?>
							<li class="product-item col-bg-3 col-lg-3 col-md-6 col-sm-4 col-xs-6 col-ts-6 style-1 post-2081 product type-product status-publish has-post-thumbnail product_cat-mac-computer product_cat-office-accessories product_cat-pc-computer product_cat-tv-audios product_tag-computer product_tag-electronic product_tag-keyboard product_tag-mouse product_tag-phone product_tag-red first instock shipping-taxable purchasable product-type-variable has-default-attributes has-children">
								<div class="product-inner equal-elem" style="height: 605px;">
								    <div class="product-thumb">
									        
									        <a href="javascript:void(0);">
							            <?/*<img class="img-responsive wp-post-image lazy" src="/gallery/listovki-m/upload/<?=$akc['photo'];?>" style="width: 240px; height: 274px;" width="240" height="274" alt="<?=$akc['name'];?>" style="display: block;">*/?>

							            <iframe src="https://www.youtube.com/embed/<?=$codeVideo;?>" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
							        </a>
									    </div>
										<div class="product-info">
											<div class="rating-wapper"></div>        
											<h3 class="product-name product_title">
												<a href="javascript:;"><?=$nameV;?></a>
											</h3>
													
												
														
									    </div>
										
								</div>
							</li>
					<? } } ?>
					</ul>
					
					</div>

			</div>

			



		</div>
	</div>
</div>


<?
	require_once '../tpl/footer.tpl';
?>