<?php
	define ('DB_HOST', 'localhost');
	define ('DB_LOGIN', 'root');
	define ('DB_PASSWORD', '');
	define ('DB_NAME', 'sale');
    // mysql_connect(DB_HOST, DB_LOGIN, DB_PASSWORD) or die ("MySQL Error: " . mysql_error());
    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASSWORD, DB_NAME, 3306);
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
	mysqli_query($mysqli, "set names utf8") or die ("<br>Invalid query: " . mysql_error());
	// mysql_select_db(DB_NAME) or die ("<br>Invalid query: " . mysql_error());

	define('H', $_SERVER['DOCUMENT_ROOT']."/");

	$error[0] = 'UNKNOWN';
	$error[1] = 'Необходимо включить куки';
	$error[2] = 'STOP';

	setcookie('err_txt', "no_error", time() + 60 * 60 * 24 * 7, "/");
  	setcookie('ok_txt', "no_ok", time() + 60 * 60 * 24 * 7, "/");

    setcookie('vakc', "0", time() + 60 * 60 * 24 * 7, "/");

	ini_set("display_errors","0");
	ini_set("display_startup_errors","0");
	ini_set('error_reporting', E_ALL);
	mb_internal_encoding('UTF-8');

	function filter($msg) {
    	//$msg = trim($msg);
    	//$msg = mysql_real_escape_string($msg);
    	return $msg;
	}

	function okText() {
        if (isset($_COOKIE['ok_txt'])) {
            if ($_COOKIE['ok_txt'] != "no_ok") {
                $txt = explode("_", $_COOKIE['ok_txt']);
                ?> <script type="text/javascript"> $.Notification.autoHideNotify('custom', 'top right', '<?=$txt[0];?>','<?=$txt[1];?>'); </script> <?
            }
        }
    }

    function errorText() {
        if (isset($_COOKIE['err_txt'])) {
            if ($_COOKIE['err_txt'] != "no_error") {
                $txt = explode("_", $_COOKIE['err_txt']);
                ?> <script type="text/javascript"> $.Notification.autoHideNotify('error', 'top right', '<?=$txt[0];?>','<?=$txt[1];?>'); </script> <?
            }
        }
    }

    function strLimit($string, $limit) {
    	$string = str_replace('????', '', $string);
    	
		$xt = iconv_strlen($string, 'UTF-8');
		$tx = iconv_substr($string, 0, $limit, 'UTF-8');
		$tx = strip_tags($tx);
		if ($xt > $limit){
			$txx= "...";
		}else{
			$txx= "";
		}
		return $tx.$txx;
    }

    function datePrePand($date) {
        $createDate = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        $dateToPay = $createDate->format('d.m.Y в H:i');
        return $dateToPay;
    }

    function datePrePand2($date) {
        $createDate = DateTime::createFromFormat('Y-m-d', $date);
        $dateToPay = $createDate->format('d-m-Y');
        return $dateToPay;
    }

    function translit5($in) {
        $trans1 = array('№', "'",'`',',',' ',"?","!",":",";",".","Ё","Ж","Ч","Ш","Щ","Э","Ю","Я","ё","ж","ч","ш","щ","э","ю","я","А","Б","В","Г","Д","Е","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ь","Ы","а","б","в","г","д","е","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ь","ы","%", "&", "\"", "+", "«", "»", "=", "’", "(", ")", "/",'\_');
        $trans2 = array('#', '-','-','-','-',"","","","",".","JO","ZH","CH","SH","SCH","Je","Jy","Ja","jo","zh","ch","sh","sch","je","jy","ja","A","B","V","G","D","E","Z","I","J","K","L","M","N","O","P","R","S","T","U","F","H","C","","Y","a","b","v","g","d","e","z","i","j","k","l","m","n","o","p","r","s","t","u","f","h","c","","y", "", "", "", "", "", "", "", "", "", "", '_');
        return str_replace($trans1, $trans2, $in);
    }

    $sql_opt_akcii = mysqli_query($mysqli, "SELECT * FROM `akcii` ORDER BY `id` DESC");
    while ($akc = mysqli_fetch_assoc($sql_opt_akcii)) {
        $n = translit5($akc['url']);
        mysqli_query($mysqli, "UPDATE `akcii` SET `url` = '$n' WHERE `id` = '$akc[id]'");
    }



    function page($k_page=1) {
        $page=1;
        if (isset($_GET['page'])) {
            if ($_GET['page']=='end')$page=intval($k_page);elseif(is_numeric($_GET['page'])) $page=intval($_GET['page']);
        }
        
        if ($page<1)$page=1;
        if ($page>$k_page)$page=$k_page;
        return $page;
    }

    function k_page($kol_foto=0,$k_p_str=10) {
        if ($kol_foto!=0) {
            $v_pages=ceil($kol_foto/$k_p_str);
            return $v_pages;
        }
        else return 1;
    }

    function navigation($link='?',$k_page=1,$page=1){
        if ($page<1)$page=1;
        echo "<nav class='woocommerce-pagination pagination'>\n";
        if ($page!=1)echo "<a class='page-numbers' href=\"".$link."page=1\"> В начало</a>";
        if ($page!=1)echo "<a class='page-numbers' href=\"".$link."page=1\">1</a>";else echo "<span aria-current='page' class='page-numbers current'>1</span>";
        for ($ot=-3; $ot<=3; $ot++){
        if ($page+$ot>1 && $page+$ot<$k_page) {
            if ($ot!=0)echo "<a class='page-numbers' href=\"".$link."page=".($page+$ot)."\">".($page+$ot)."</a>";else echo " <span aria-current='page' class='page-numbers current'>".($page+$ot)."</span>";
        if ($ot==3 && $page+$ot<$k_page-1)echo "<span>...</span>";}}
        if ($page!=$k_page)echo " <a class='page-numbers' href=\"".$link."page=end\">$k_page</a>";elseif ($k_page>1)echo " <span aria-current='page' class='page-numbers current'>$k_page</span>";
        if ($page!=$k_page)echo " <a class='page-numbers' href=\"".$link."page=end\">Последняя</a>";
        echo "</nav>";
    }

    if (isset($_COOKIE['my_cityes'])) {
        $my_city = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM `cites` WHERE `id` = '$_COOKIE[my_city]' LIMIT 1"));
        if (!$my_city['id']) {
            $my_cityes = "3";
            $my_city = 3;
        }else{
            $my_city = $my_city['id'];
            $my_cityes = $_COOKIE['my_cityes'];
        
        }
    }else{
        setcookie('my_city', "3", time() + 60 * 60 * 24 * 7, "/");
        setcookie('my_cityes', "3", time() + 60 * 60 * 24 * 7, "/");
        $my_city = 3;
        $my_cityes = "3";
    }

    $sand = "AND (";

    $wand = "WHERE ("; //  `id_city` = '$my_city'
    $omysite["name"] = "";
    foreach (explode(", ", $my_cityes) as $city) {
        $city = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM `cites` WHERE `id` = '$city' LIMIT 1"));
        $omysite["name"] .= $city["name"] . ", ";
        $sand .= " `id_city` = '" . $city . "' OR";
        $wand .= " `id_city` = '" . $city . "' OR";
    }
    
    $omysite["name"] = substr($omysite["name"], 0, -2);
    $sand = substr($wand, 0, -2) . ")";
    $wand = substr($wand, 0, -2) . ")";

    // var_dump($wand);