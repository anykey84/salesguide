<?php
	require_once 'core/db.php';

	$title = "О компании | SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве";
	$desc = "SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве";
	$keys = "#скидки, #акции, #распродажи, #купоны, #промокоды, #скидкимосква, #скидкикраснодар, #акциикраснодар, #акциимосква, #распродажикраснодар, #распродажимосква";


	require_once 'tpl/header.tpl';
?>


<main class="site-main  main-container no-sidebar">
        <div class="container">
			<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"><h2 class="trail-browse">Browse:</h2><ul class="trail-items breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="trail-item trail-begin"><a href="/" rel="home"><span itemprop="name">Главная</span></a><meta itemprop="position" content="1" /></li><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="trail-item active"><a href="https://m-electronics.kute-themes.com/elements/"><span itemprop="name">О компании</span></a><meta itemprop="position" content="2" /></li></ul></nav>            <div class="row">
                <div class="main-content col-sm-12">
                    <h2 class="page-title">
						О компании                    </h2>
					                            <div class="page-main-content">
								<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element ovic_custom_5a977689b7d3c vc_custom_1517969091300 " >
		<div class="wpb_wrapper">
			

			<p>
				
				SALE.GUIDE - ваш друг и проводник в мире распродаж. Каждый день мы работаем над тем, чтобы
собрать на нашем портале все скидки, распродажи и акции именно в вашем городе. Нам мало
сидеть и ждать пока придет письмо через авторассылку. Мы сами выезжаем на место, и снимаем
видео и фоторепортажи. Увидим своими глазами, потрогаем, опробуем и – обязательно –
расскажем вам!
			</p>

			<p>Команда SALE.GUIEDE стремится охватить весь спектр услуг и товаров, интересных простому
потребителю. От автомоек и магазинов техники – до ресторанов и шоурумов с брендовой
одеждой. Наши репортеры всегда в центре событий. Наши менеджеры ведут прямой диалог с
бизнесом и предлагают им проведение совместных акций интересных и выгодных вам.</p>

<p>SALE.GUIDE существует во всех актуальных социальных сетях в удобном для пользователей
формате. Наши редакторы следят за доступностью информации и в постоянном режиме
публикуют свежие новости о скидках, акциях и распродажах в Instagram, ВКонтакте, на Facebook и
нашем официальном YouTube-канале.</p>

<p>Члены клуба SALE.GUIDE получают секретную рассылку с акциями, которых еще нет на портале, а
также могут приобретать купоны, которые позволят сэкономить ощутимую сумму в магазинах,
барах, ресторанах, автомастерских, салонах красоты и не только!</p>

<p>Не упустите свою выгоду! Добро пожаловать на SALE.GUIDE!</p>

		</div>
	</div>

	
                            </div>
														                </div>
				            </div>
        </div>
    </main>

<?
	require_once 'tpl/footer.tpl';
?>