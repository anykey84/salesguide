
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                        	<li class="text-muted menu-title">Главное меню</li>

                            <li class="has_sub">
                                <a href="/control/cites.php" class="waves-effect"><i class="ti-home"></i> <span> Список городов </span></a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-exchange-vertical"></i> <span> Категории </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/control/all-category.php">Все категори</a></li>
                                    <li><a href="/control/new-category.php">Добавить новую</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span> Акции </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/control/all-post.php">Все акции</a></li>
                                    <li><a href="/control/new-post.php">Добавить новую</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span> Страницы </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/control/pages-all.php">Все страницы</a></li>
                                    <li><a href="/control/pages-new.php">Создать новую</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span> Каталоги </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/control/all-listovki.php">Все магазины</a></li>
                                    <li><a href="/control/listovki-new.php">Добавить новый</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span> Розыгрыши </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/control/all-roz.php">Все розыгрыши</a></li>
                                    <li><a href="/control/roz-new.php">Добавить новый</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span> Фотогид </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/control/all-fotogid.php">Все разделы</a></li>
                                    <li><a href="/control/fotogid-new.php">Добавить новый</a></li>
                                </ul>
                            </li>

                            

                            

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
