<?php
	require_once 'core/db.php';

	$title = "Вакансии | SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве";
	$desc = "SALE.GUIDE все о скидках, акциях и распродажах в Краснодаре, Москве";
	$keys = "#скидки, #акции, #распродажи, #купоны, #промокоды, #скидкимосква, #скидкикраснодар, #акциикраснодар, #акциимосква, #распродажикраснодар, #распродажимосква";

	require_once 'tpl/header.tpl';
?>



<main class="site-main  main-container no-sidebar">
        <div class="container">
			<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"><h2 class="trail-browse">Browse:</h2><ul class="trail-items breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="trail-item trail-begin"><a href="/" rel="home"><span itemprop="name">Главная</span></a><meta itemprop="position" content="1" /></li><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="trail-item active"><a href="https://m-electronics.kute-themes.com/elements/"><span itemprop="name">Вакансии</span></a><meta itemprop="position" content="2" /></li></ul></nav>            <div class="row">
                <div class="main-content col-sm-12">
                    <h2 class="page-title">
						Все вакансии                    </h2>
					                            <div class="page-main-content">
								<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<h4 style="font-size: 18px;text-align: left" class="vc_custom_heading ovic_custom_5a977689b7cf4 vc_custom_1519875702182" >SEО и SMM</h4>
	<div class="wpb_text_column wpb_content_element ovic_custom_5a977689b7d3c vc_custom_1517969091300 " >
		<div class="wpb_wrapper">
			<p>З/П от 25 000 руб. на руки</p>
			<p>Краснодар (Ростовское шоссе/Солнечная)<br>
			Требуемый опыт работы: 3–6 лет<br>Полная занятость, полный день</p>

			<p>
				<ul><b>Обязанности:</b>
					<li>Ведение и наполнение соцсетей (Инстаграм, Фейсбук, Ютуб, ВКонтакте)</li>
					<li>Наполнение и ведение сайта компании.</li>
				</ul>

				<ul><b>Требования:</b>
					<li>Опыт работы в социальных сетях.</li>
					<li>Умение работать в программах для видеомонтажа</li>
					<li>Владение Adobe Photoshop</li>
				</ul>

				<ul><b>Условия:</b>
					<li>5/2</li>
					<li>На территории работодателя.</li>
					<li>Полный рабочий день.</li>
				</ul>

			</p>

		</div>
	</div>

	<hr><h4 style="font-size: 18px;text-align: left" class="vc_custom_heading ovic_custom_5a977689b7cf4 vc_custom_1519875702182" >Менеджер по работе с клиентами</h4>
	<div class="wpb_text_column wpb_content_element ovic_custom_5a977689b7d3c vc_custom_1517969091300 " >
		<div class="wpb_wrapper">
			<p>З/П от 25 000 до 60 000 руб. на руки</p>
			<p>Краснодар (Ростовское шоссе/Солнечная)<br>Требуемый опыт работы: 3–6 лет<br>Полная занятость, полный день</p>

			<p>
				<ul><b>Обязанности:</b>
					<li>поиск клиентов</li>
					<li>создание и ведение клиентской базы</li>
					<li>ведение переговоров с клиентами</li>
					<li>продажа услуг компании (реклама)</li>
				</ul>

				<ul><b>Требования:</b>
					<li>уверенный пользователь ПК</li>
					<li>грамотная речь</li>
				</ul>

				<ul><b>Условия:</b>
					<li>5/2</li>
					<li>с 9 до 18 ч.</li>
				</ul>

			</p>

		</div>
	</div>

	<hr><h4 style="font-size: 18px;text-align: left" class="vc_custom_heading ovic_custom_5a977689b7cf4 vc_custom_1519875702182" >Репортер</h4>
	<div class="wpb_text_column wpb_content_element ovic_custom_5a977689b7d3c vc_custom_1517969091300 " >
		<div class="wpb_wrapper">
			<p>З/П до 25 000 руб. на руки</p>
			<p>Краснодар (Ростовское шоссе/Солнечная)<br>Требуемый опыт работы: не требуется<br>Полная занятость, полный день</p>

			<p>
				<ul><b>Обязанности:</b>
					<li>Поиск новостей, фото и видеосъемка на телефон, публикация в соцсетях.</li>
				</ul>

				<ul><b>Требования:</b>
					<li>Девушка 18-30 лет</li>
					<li>Владение ПК – уверенный пользователь</li>
					<li>Навыки управления аккаунтами в соцсетях (ВКонтакте, Facebook, Instagram, YouTube)</li>
					<li>Владение фото и видеокамерой на уровне пользователя</li>
					<li>Знание современных тенденций, брендов, цен на товары, услуги</li>
					<li>Телефон с постоянным доступом в интернет и камерой не ниже 12 мП</li>
					<li>Правильная речь</li>
					<li>Грамотность</li>
					<li>Чувство юмора</li>
					<li>Возможность посещать торговые центры</li>
				</ul>

				<ul><b>Условия:</b>
					<li>5/2</li>
					<li>С 9 до 18 ч</li>
				</ul>

			</p>

		</div>
	</div>

</div></div></div></div>
                            </div>
														                </div>
				            </div>
        </div>
    </main>



<?
	require_once 'tpl/footer.tpl';
?>